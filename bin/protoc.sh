#!/bin/bash

getto_codes_grpc_protoc_main(){
  local lib
  local root
  local files
  local file

  lib=lib

  echo "generate grpc..."

  root=$PROTOC_ROOT/lib

  if [ -d "$root" ]; then
    files=$(find $root -type f -name *.proto)

    for file in $files; do
      echo ${file#$root/}
    done

    bundle exec grpc_tools_ruby_protoc -I "$root" --ruby_out=$lib --grpc_out=$lib $files
  fi

  echo "done"
}

getto_codes_grpc_protoc_main "$@"
